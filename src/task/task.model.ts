import * as mongoose from 'mongoose';
import { IUser } from './task.interface';

const userSchema: mongoose.Schema = new mongoose.Schema(
    {
        user: String,
        tasks: [{_id: false, task: String, startTime: String, endTime: String, time: String}],
        startTime: String,
        endTime: String,
        checkoutStatus: Boolean
    }
);

export const UserModel = mongoose.model<IUser & mongoose.Document>('User', userSchema);
