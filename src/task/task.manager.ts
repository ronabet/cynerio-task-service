import { intervalToDuration } from 'date-fns';
import {IUser, taskObj } from './task.interface';
import { TaskRepository } from './task.repository';


export class TaskManager {
    static async userCheckIn(user: IUser, taskObj: taskObj) {
        if(await TaskRepository.findUser(user.user)){
            return TaskRepository.updateTasksArr(user.user, taskObj);
        }
        return TaskRepository.createCheckIn(user);
    }

    static async checkOut(userName: String, endTime: string) {   
        const user: any = await TaskRepository.findUser(userName)
        const timeDistance = intervalToDuration({
            start: new Date(user.tasks[user.tasks.length - 1 ].startTime),
            end: new Date(endTime)
        }).hours;
        user.tasks[user.tasks.length - 1 ].endTime = endTime;
        user.tasks[user.tasks.length - 1 ].time = timeDistance
        user.checkoutStatus = true;

        return TaskRepository.updateUser(user);
    }

    static async getUserState(user: String) {
        return TaskRepository.findUser(user);
    }

    static getReport () {
        return TaskRepository.getAllUsersTasks();
    }
}
