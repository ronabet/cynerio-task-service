import { Router } from 'express';
import { TaskRouter } from './task/task.router';

const AppRouter: Router = Router();

AppRouter.get('/isalive', (req, res) => {
    res.send('alive');
});
AppRouter.use('/api/taskservice', TaskRouter);

export { AppRouter };
