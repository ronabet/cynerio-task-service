import * as express from 'express';
import * as http from 'http';
import { config } from './config';
import { AppRouter } from './router';

export class Server {
    public app: express.Application;
    private server: http.Server;

    public static bootstrap(): Server {
        return new Server();
    }

    private constructor() {
        this.app = express();
        this.app.use(express.json());
        this.app.use(express.urlencoded({
          extended: true
        }));

        this.app.use(AppRouter);

        this.server = http.createServer(this.app);
        this.server.listen(config.port, () => {
            console.log(
                `Server running on port ${config.port}`
            );
        });
        
    }
}

